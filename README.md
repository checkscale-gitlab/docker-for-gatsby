# Docker for Gatsby

This Docker template is meant to run Gatsby applications, providing up-to-date software and tools to manage it.

## Popular releases
- [v3.14.2](https://gitlab.com/deviktta/docker-for-gatsby/-/releases/3.14.2): latest release for Gatsby 3.14 with Apache 2 web server.

## How to use this Docker template

### Requirements:
The only requirement is to have installed the last version of both [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/). 

### Installation:
1. Clone this project from Gitlab (`git clone https://gitlab.com/deviktta/docker-for-gatsby.git`) or [download a release](https://gitlab.com/deviktta/docker-for-gatsby/-/releases). 

2. Bring up the containers: `docker-compose up -d --build`

3. Install existing Gatsby app:
 - Clone your Gatsby project into /gatsby-root folder, making sure your package.json is located in the root level.
 - Install your Gatsby project: usually by downloading NodeJS dependencies `./docker/scripts/yarn install`, but further steps could be taken depending on your project.
 - Run your Gatsby app in dev mode: `./docker/scripts/gatsby develop -H 0.0.0.0 -p 8001`.

Or, if you wish, you can omit step 3 and create a new Gatsby app:
  - Create a new Gatsby project: `./docker/scripts/gatsby init hello-world`.
  - Set proper permissions to the folder directory: `sudo chown $USER gatsby-root/ -R`.
  - Because Gatsby CLI doesn't allow to create the project in the root directory, we must move the folder contents one level up: `mv gatsby-root/hello-world/* gatsby-root/hello-world/.* gatsby-root/ && rm gatsby-root/hello-world -r`.

4. Run your Gatsby application in develop mode: `./docker/scripts/gatsby develop -H 0.0.0.0 -p 8001` and visit your site by navigating to the given _External URL_ by Gastby cli.

### Managing your Gatsby application:
**Available tools:**
* Gatsby cli: `./docker/scripts/gatsby`
* Yarn: `./docker/scripts/yarn`
* NPM: `./docker/scripts/npm`

Overriding your Docker environment file (copy `/.env` file to `/.env.local`) and customizing the values is advised.

**Production mode:**
- Build your Gatsby application `./docker/scripts/gatsby build`
- Then, access it through Apache navigating to http://localhost.

## License
This project is under [MIT License](https://gitlab.com/deviktta/docker-for-gatsby/-/blob/master/LICENSE.txt).

## About
Contact e-mail: [deviktta@protonmail.com](mailto:deviktta@protonmail.com); made with love from Barcelona, Catalunya.
